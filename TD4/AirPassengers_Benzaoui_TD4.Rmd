---
title: "TD4_AirPassengers"
author: "Benzaoui Mohamed - Benabbou Halima"
date: "11 février 2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r cars}
summary(cars)
```

## Read data
To read the synthetic ping-pong measurements, the code below reads the file `AirPassengers.csv` and creates the data frame `df`. This data frame has a single column called `time` which contains all measurements that have been previously generated.  The call to `head(df)` lists the first rows of the data frame.

```{r}
df <- read.csv("data/AirPassengers.csv", header=TRUE)
head(df);
```


## Including Plots

You can also embed plots, for example:

```{r}
library(ggplot2);
ggplot(data=df, aes(x=time, y=AirPassengers)) +
  geom_smooth(methods=lm, se=FALSE, color="darkred") +
  geom_point(size = 1)+geom_line()+
  labs(title="Number of passengers over the years") +
  ylim(c(0,650));
```

  Le nombre de passagers augmente de manière alternative. Mais une croissance non linéaire est malgré tout remarquée.   
